CC=gcc

zad1 : zad1.o mediana.o srednia.o odchylenie.o
	$(CC) -Wall -o zad1 zad1.o mediana.o srednia.o odchylenie.o -lm
zad1.o : zad1.c funkcja.h
	$(CC) -Wall -c zad1.c
mediana.o : mediana.c
	$(CC) -Wall -c mediana.c
srednia.o : srednia.c
	$(CC) -Wall -c srednia.c
odchylenie.o : odchylenie.c
	$(CC) -Wall -c odchylenie.c
