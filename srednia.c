#include <stdio.h>
#include "funkcja.h"

float sredniunia(float tab[], int size)
{
    float suma = 0.0;
    for(int i=0; i<size; i++)
    {
        suma += tab[i];
    }
    return suma / size;
}