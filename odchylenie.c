#include <stdio.h>
#include <math.h>
#include "funkcja.h"

float odchylonko(float *tab, float sr, int size)
{
    float wariancja1 = 0;
    float wariancja2 = 0;
    for(int i=0; i<size; i++)
    {
        wariancja1 += pow(tab[i] - sr, 2);
    }
    wariancja2 = wariancja1 / 50;
    return sqrt(wariancja2);
}