#include <stdio.h>
#include "funkcja.h"

float medianka(float *tab, int size)
{
    float mediana = 0.0;
    if(size%2!=0)
        mediana = tab[(size+1)/2];
    else
        mediana = (tab[size/2] + tab[(size/2)+1])/2;
    return mediana;
}