#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "funkcja.h"

void sort (float *tab, int size)
{
    float temp = 0.0;
    for(int i=0; i<size-1; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if (tab[j] > tab[j+1])
            {
                temp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = temp;
            }
        }    
    }
    return;
}

int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.rec", "r");

    int size = 50;

    float *X;
    X = (float *)calloc(size, sizeof(float));

    float *Y;
    Y = (float *)calloc(size, sizeof(float));

    float *RHO;
    RHO = (float *)calloc(size, sizeof(float));

    char smieci[60] = {};

    for(int i=0; i<4;i++)
    {
        fscanf(plik, "%s", smieci);
    }

    for(int i=0; i<size; i++)
    {
        fscanf(plik, "%s", smieci);
        fscanf(plik, "%f", &X[i]);
        fscanf(plik, "%f", &Y[i]);
        fscanf(plik, "%f", &RHO[i]);
    }
    fclose(plik);
    
    sort(X, size);
    printf("Mediana X wynosi %.3f\n", medianka(X, size));

    sort(Y, size);
    printf("Mediana Y wynosi %.3f\n", medianka(Y, size));

    sort(RHO, size);
    printf("Mediana RHO wynosi %.3f\n", medianka(RHO, size));


    float sredniaX = sredniunia(X, size);
    float sredniaY = sredniunia(Y, size);
    float sredniaRHO = sredniunia(RHO, size);

    printf("Srednia X wynosi %.3f\n", sredniunia(X, size));
    printf("Srednia Y wynosi %.3f\n", sredniunia(Y, size));
    printf("Srednia RHO wynosi %.3f\n", sredniunia(RHO, size));
    printf("Odchylenie standardowe X wynosi %.3f\n", odchylonko(X, sredniaX, size));
    printf("Odchylenie standardowe Y wynosi %.3f\n", odchylonko(Y, sredniaY, size));
    printf("Odchylenie standardowe RHO wynosi %.3f\n", odchylonko(RHO, sredniaRHO, size));

    plik = fopen("P0001_attr.rec", "a");
    fprintf(plik, "\n\n");
    fprintf(plik, "Srednia:\nX\tY\tRHO\n%.3f\t%.3f\t%.3f\n", sredniunia(X, size), sredniunia(Y, size), sredniunia(RHO, size));
    fprintf(plik, "Mediana:\nX\tY\tRHO\n%.3f\t%.3f\t%.3f\n", medianka(X, size), medianka(Y, size), medianka(RHO, size));
    fprintf(plik, "Odchylenie standardowe:\nX\tY\tRHO\n%.3f\t%.3f\t%.3f\n", odchylonko(X, sredniaX, size), odchylonko(Y, sredniaY, size), odchylonko(RHO, sredniaRHO, size));
    
    fclose(plik);
    free(X);
    free(Y);
    free(RHO);
    return 0;
}